<?php

namespace Kdk\Generator\Specification;

/**
 * Abstract specification
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
abstract class CompositeSpecification implements SpecificationInterface
{
    /**
     * @inheritdoc
     */
    public function _and(SpecificationInterface $other) 
    {
        return new AndSpecification($this, $other);
    }

    /**
     * @inheritdoc
     */
    public function _or(SpecificationInterface $other) 
    {
        return new OrSpecification($this, $other);
    }

    /**
     * @inheritdoc
     */
    public function _not() 
    {
       return new NotSpecification($this);
    }
}
