<?php

namespace Kdk\Generator\Specification;

use Kdk\Game\Draw;

/**
 * Specification check that any number in current draw
 * exists in last draw of Game.
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
class ExistsSpecification extends CompositeSpecification
{
    /**
     * @var Kdk\Game\Draw
     */
    private $last;
    
    public function __construct(Draw $last)
    {
        $this->last = $last;
    }

    /**
     * @inheritdoc
     */
    public function isSatisfiedBy(Draw $draw)
    {
        foreach($draw->getNumbers() as $number){
            if(in_array($number, $this->last->getNumbers())){
                return true;
            }
        }
        
        return false;
    }

}
