<?php

namespace Kdk\Generator\Specification;

use Kdk\Game\Draw;

/**
 * Specification for OR condition.
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
class OrSpecification extends CompositeSpecification
{
    /**
     * @var SpecificationInterface
     */
    private $first;
    
    /**
     * @var SpecificationInterface 
     */
    private $second;
    
    public function __construct(SpecificationInterface $first, SpecificationInterface $second)
    {
        $this->first = $first;
        $this->second = $second;
    }

    /**
     * @inheritdoc
     */
    public function isSatisfiedBy(Draw $draw)
    {
        return $this->first->isSatisfiedBy($draw) || $this->second->isSatisfiedBy($draw);
    }
}
