<?php

namespace Kdk\Generator\Specification;

use \Kdk\Game\Draw;

/**
 * Specificatin pattern for generated numbers.
 * Not in, grater than etc.
 * 
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
interface SpecificationInterface
{
    /**
     * Check if draw satisfied specification
     * @param Draw $draw
     * @return boolean
     */
    public function isSatisfiedBy(Draw $draw);
    
    /**
     * And condition
     * 
     * @param \Kdk\Specification\SpecificationInterface $other
     * @return \Kdk\Specification\AndSpecification
     */
    public function _and(SpecificationInterface $specification);
    
    /**
     * Or condition
     * @param \Kdk\Specification\SpecificationInterface $other
     * @return \Kdk\Specification\OrSpecification
     */
    public function _or(SpecificationInterface $specification);
    
    /**
     * Not condition
     * 
     * @return \Kdk\Specification\NotSpecification
     */
    public function _not();
}
