<?php

namespace Kdk\Generator\Specification;

use Kdk\Game\Draw;

/**
 * Specification for NOT condition.
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
class NotSpecification extends CompositeSpecification
{
    /**
     * @var SpecificationInterface
     */
    private $wrapped;
    
    public function __construct(SpecificationInterface $wrapped)
    {
        $this->wrapped = $wrapped;
    }

    /**
     * @inheritdoc
     */
    public function isSatisfiedBy(Draw $draw)
    {
        return !$this->wrapped->isSatisfiedBy($draw);
    }
}
