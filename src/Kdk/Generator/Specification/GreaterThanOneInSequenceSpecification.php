<?php

namespace Kdk\Generator\Specification;

use Kdk\Game\Draw;

/**
 * Specification for numbers in sequence where two numbers next to
 * should to be grather than one.
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
class GreaterThanOneInSequenceSpecification extends CompositeSpecification
{
    /**
     * @inheritdoc
     */
    public function isSatisfiedBy(Draw $draw)
    {
        $numbers = $draw->getNumbers();
                
        for($i=0; $i<count($numbers); $i++){
            if($numbers[$i] + 1 == $numbers[$i+1]){
                return false;
            }
        }
        
        return true;
    }
}
