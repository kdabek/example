<?php

namespace Kdk\Game;

/**
 * Draw of the game.
 * It has unique id, date of draw and numbers.
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
class Draw
{
    /**
     * @var int
     */
    private $id;
    
    /**
     * @var \Datetime
     */
    private $date;
    
    /**
     * @var int[]
     */
    private $numbers = array();
    
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setDate(\Datetime $date)
    {
        $this->date = $date;
        return $this;
    }
        
    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function addNumber($number)
    {
        if(in_array($number, $this->numbers)){
            throw new \RuntimeException('The number '.$number.' is exists in draw.');
        }
        
        array_push($this->numbers, $number);
    }
    
    public function getNumbers()
    {
        return $this->numbers;
    }

}
