Feature: Choose numbers to generate
    As a User
    I want to choose which numbers will not be generated
    So that numbers is no fully random

Scenario: Check that two next to numbers in sequence is greater than one
    Given there is a Draw with numbers:
    | numbers |
    |1,3,5,7,9|
    When I use "GreaterThanOneInSequence" specification
    Then Numbers it should be ok

Scenario: Check that some next to numbers in sequence is equal one
    Given there is a Draw with numbers:
    | numbers |
    |1,3,4,7,9|
    When I use "GreaterThanOneInSequence" specification
    Then Numbers it should not be ok
    And I have to generate another sequence


Scenario: Check that any number in current Draw exists in last Draw
    Given there is a Draw with numbers:
    | numbers |
    |1,3,4,7,9|
    And there is a last Draw with numbers:
    | numbers |
    |10,11,4,22,23|
    When I use "Exists" specification with last Draw
    Then Numbers it should be ok

Scenario: Check that two next to numbers in sequence is greater than one and not exists in last draw
    Given there is a Draw with numbers:
    | numbers |
    |1,3,5,7,9|
    And there is a last Draw with numbers:
    | numbers |
    |10,11,6,22,23|
    When I use "GreaterThanOneInSequence" specification
    And I use not "Exists" specification with last Draw
    Then Numbers it should be ok

Scenario: Check that two next to numbers in sequence is greater than one or exists in last draw
    Given there is a Draw with numbers:
    | numbers |
    |1,3,4,7,9|
    And there is a last Draw with numbers:
    | numbers |
    |10,11,9,22,23|
    When I use "GreaterThanOneInSequence" specification
    And I use or "Exists" specification with last Draw
    Then Numbers it should be ok