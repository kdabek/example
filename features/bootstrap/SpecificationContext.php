<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Kdk\Game\Draw;

/**
 * Defines application features from the specific context.
 */
class SpecificationContext implements Context, SnippetAcceptingContext
{
    /**
     * @var Kdk\Game\Draw
     */
    private $draw;
    
    /**
     * @var Kdk\Game\Draw
     */
    private $last;
    
    /**
     * @var Kdk\Generator\Specification\SpecificationInterface
     */
    private $specification;
    
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->draw = new Draw();
    }

    public function fillNumbers(Draw $draw, TableNode $table)
    {
        foreach ($table as $row) {
            $numbers = explode(',', $row['numbers']);
            
            foreach($numbers as $number){
                $draw->addNumber($number);
            }
        }
        
    }
    
    public function getSpecification($name, $arg = null)
    {
        $specification = "Kdk\\Generator\\Specification\\".$name."Specification";
        return new $specification($arg);
    }
    
    /**
     * @Given there is a Draw with numbers:
     */
    public function thereIsADrawWithNumbers(TableNode $table)
    {
        $this->fillNumbers($this->draw, $table);
    }

    /**
     * @When I use :arg1 specification
     */
    public function iUseSpecification($arg1)
    {
        $this->specification = $this->getSpecification($arg1);
    }

    /**
     * @Then Numbers it should be ok
     */
    public function numbersItShouldBeOk()
    {
        PHPUnit_Framework_Assert::assertTrue($this->specification->isSatisfiedBy($this->draw));
    }

    /**
     * @Then Numbers it should not be ok
     */
    public function numbersItShouldNotBeOk()
    {
        PHPUnit_Framework_Assert::assertFalse($this->specification->isSatisfiedBy($this->draw));
    }

    /**
     * @Then I have to generate another sequence
     */
    public function iHaveToGenerateAnotherSequence()
    {
        //Generate new sequence of numbers
    }
    
    /**
     * @Given there is a last Draw with numbers:
     */
    public function thereIsALastDrawWithNumbers(TableNode $table)
    {
        $this->last = new Draw();
        $this->fillNumbers($this->last, $table);
    }

    /**
     * @When I use :arg1 specification with last Draw
     */
    public function iUseSpecificationWithLastDraw($arg1)
    {
        $this->specification = $this->getSpecification($arg1, $this->last);
    }

    /**
     * @When I use not :arg1 specification with last Draw
     */
    public function iUseNotSpecificationWithLastDraw($arg1)
    {
        $specification = $this->getSpecification($arg1, $this->last);
        $this->specification = $this->specification->_and($specification->_not());
    }
    

    /**
     * @When I use or :arg1 specification with last Draw
     */
    public function iUseOrSpecificationWithLastDraw($arg1)
    {
        $specification = $this->getSpecification($arg1, $this->last);
        $this->specification = $this->specification->_or($specification);
    }
}
