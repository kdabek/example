<?php

use Kdk\Game\Draw;
use Kdk\Generator\Specification\GreaterThanOneInSequenceSpecification;
use Kdk\Generator\Specification\ExistsSpecification;

/**
 * Test specifications of generator.
 *
 * @author Krystian Dąbek <krystian.dk@gmail.com>
 */
class SpecificationTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Kdk\Game\Draw 
     */
    private $draw;
    
    public function setUp()
    {
        $this->draw = new Draw();
    }
    
    public function prepareDraw(Draw $draw, array $numbers)
    {
        foreach ($numbers as $number){
            $draw->addNumber($number);
        }
        
        return $draw;
    }
    
    public function testNextToNumbersGreaterThanOneInSequence()
    {
        $this->prepareDraw($this->draw, array(1,3,5,7,9));
        $specification = new GreaterThanOneInSequenceSpecification();
        $this->assertTrue($specification->isSatisfiedBy($this->draw));
        
    }
    
    public function testNextToNumbersEqualsOneInSequence()
    {
        $this->prepareDraw($this->draw, array(1,3,4,7,9));
        $specification = new GreaterThanOneInSequenceSpecification();
        $this->assertFalse($specification->isSatisfiedBy($this->draw));
    }
    
    public function testNumbersNotExistsInLastDraw()
    {
        $this->prepareDraw($this->draw, array(1,3,5,7,9));
        $lastDraw = $this->prepareDraw(new Draw(), array(10,11,15,22,23));
        $specification = new ExistsSpecification($lastDraw);
        $this->assertFalse($specification->isSatisfiedBy($this->draw));
    }
    
    public function testNumbersExistsInLastDraw()
    {
        $this->prepareDraw($this->draw, array(1,3,5,7,9));
        $lastDraw = $this->prepareDraw(new Draw(), array(10,11,5,22,23));
        $specification = new ExistsSpecification($lastDraw);
        $this->assertTrue($specification->isSatisfiedBy($this->draw));
    }
    
    public function testBothAndNot()
    {
        $this->prepareDraw($this->draw, array(1,3,5,7,9));
        $lastDraw = $this->prepareDraw(new Draw(), array(10,11,15,22,24));
        $greater = new GreaterThanOneInSequenceSpecification();
        $exists = new ExistsSpecification($lastDraw);
        
        $this->assertTrue($greater->_and($exists->_not())->isSatisfiedBy($this->draw));
    }
    
    public function testAll()
    {
        $this->prepareDraw($this->draw, array(1,3,4,7,9));
        $lastDraw = $this->prepareDraw(new Draw(), array(1,11,15,22,24));
        $orDraw = $this->prepareDraw(new Draw(), array(9,10,13,23,25));
        $greater = new GreaterThanOneInSequenceSpecification();
        $exists = new ExistsSpecification($lastDraw);
        $orExists = new ExistsSpecification($orDraw);
        
        $this->assertTrue($greater->_and($exists->_not())->_or($orExists)->isSatisfiedBy($this->draw));
    }
    
}
