# README #

Przykładowy kod będący częścią generatora liczb Lotto, który odpowiada za sprawdzanie warunku lub grupy warunków.

Kod implementuje wzorzec projektowy "Specyfikacja" wywodzący się z DDD.

Za pomocą takiego zestawu klas można otrzymać bardziej czytelny i dynamiczny kodzamiast szeregu instrukcji warunkowych. Instrukcje można wykorzystać ponownie do innych celów.

### Struktura plików i klas ###

* bin - pliki uruchomieniowe testów
* features - scenariusze testów
* src - pliki źródłowe w tym klasy:
    * *Draw* - klasa zawierająca dane losowania i wylosowane/wygenerowane liczby
    * *SpecificationInterface* - Interfejs określający że klasy implementujące muszą posiadać metodę **isSatisfiedBy**, w której sprawdzane będą warunki dla obiektu przesłanego w parametrze, w tym wypadku obiektu *Draw*, oraz metody: *_and, _or, _not* - te ostatnie będą jednak zaimplementowane już w klasie abstrakcyjnej
    * *CompositeSpecification* - klasa abstrakcyjna która implementuje metody *_and, _or, _not* które zwracają kolejne klasy dziedziczące po *CompositeSpecification* dzięki czemu można wykorzystać łączenie metod
    * *AndSpecification* - klasa sprawdzająca czy dwie specyfikacje są spełnione
    * *OrSpecification* - klasa sprawdzająca czy którakolwiek ze specyfikacji jest spełniona
    * *NotSpecification* - klasa sprawdzająca czy specyfikacja jest niespełniona
    * *Exists* - klasa sprawdza czy którekolwiek numery z przesłanego losowania występują w uprzednio ustawionym losowaniu
    * *GreaterThanOneInSequenceSpecification* - klasa sprawdza czy dwie liczby w tablicy występujące obok siebie są większe o conajmniej 1
* tests - testy jednostkowe

### Uruchomienie ###

    composer install
    bin/behat
